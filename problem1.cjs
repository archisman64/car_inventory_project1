function identifyCarById(inventory, carId) {
    if((inventory && inventory.length === 0) || (carId!== 0 && !carId) || !inventory || !Array.isArray(inventory) || typeof carId !== 'number') {
        console.log('could not find the car');
        return [];
    }
    for(let index = 0; index < inventory.length; ++index) {
        if(inventory[index].id === carId) {
            console.log('found the car');
            const car = inventory[index];
            return [car];
        }
    }
};

module.exports = identifyCarById;