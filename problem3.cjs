// const inventory = require('./inventory.cjs');

function sortCarsByModel(inventory) {
    return inventory.sort((a, b) => a.car_model.toLowerCase() > b.car_model.toLowerCase() ? 1 : -1);
}
// const solution = sortCarsByModel(inventory);
// console.log(solution);

module.exports = sortCarsByModel;