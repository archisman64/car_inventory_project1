// const inventory = require('./inventory.cjs')

function getCarYears(inventory) {
    let yearData = [];
    for(let index = 0; index < inventory.length; ++index) {
        yearData.push(inventory[index].car_year);
    }
    // console.log(yearData);
    return yearData;
}

// getCarYears(inventory);

module.exports = getCarYears;