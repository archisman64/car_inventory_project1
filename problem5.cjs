function findCarsOlderThan2000(inventory, yearData) {
    let carCount = 0;
    for(let index = 0; index < yearData.length; ++index) {
        if(yearData[index] < 2000) {
            carCount++;
        }
    } 
    let requiredCars = [];
    for(let index = 0; index < inventory.length; ++index) {
        if(inventory[index].car_year < 2000) {
            requiredCars.push(inventory[index]);
        }
    }
    return {carCount, requiredCars};
}

module.exports = findCarsOlderThan2000;