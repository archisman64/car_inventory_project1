function find_BMW_AUDI(inventory) {
    const requiredCars = [];
    for(let index = 0; index < inventory.length; ++index) {
        const car = inventory[index];
        if(car.car_make.toLowerCase() === 'bmw' || car.car_make.toLowerCase === 'audi') {
            requiredCars.push(car);
        }
    }
    return requiredCars;
}

module.exports = find_BMW_AUDI;