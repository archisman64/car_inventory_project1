const inventory = require('../data/inventory.cjs');
const sortCarsByModel = require('../problem3.cjs');

const sortedCarsArray = sortCarsByModel(inventory);
console.log('Cars sorted by their model name:', sortedCarsArray);