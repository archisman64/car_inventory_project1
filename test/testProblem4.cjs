const inventory = require('../data/inventory.cjs');
const getCarYears = require('../problem4.cjs');

const yearData = getCarYears(inventory);
console.log('Make-year of all the cars:', yearData);