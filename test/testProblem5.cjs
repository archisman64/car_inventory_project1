const inventory = require('../data/inventory.cjs');

const yearData = require('../problem4.cjs')(inventory);

const findCarsOlderThan2000 = require('../problem5.cjs');

const result = findCarsOlderThan2000(inventory, yearData); // returns an object with count of cars having make-year less than 2000 and the list of those cars

console.log(`Number of cars having make-year less than 2000: ${result.carCount}`);
console.log(`List of cars having make-year less than 2000: ${JSON.stringify(result.requiredCars)}`);
console.log(`Length of required cars list: ${result.requiredCars.length}`);