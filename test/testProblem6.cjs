const find_BMW_AUDI = require('../problem6.cjs');
const inventory = require('../data/inventory.cjs');

const carsArray = find_BMW_AUDI(inventory);

console.log('Required Cars (BMW and Audi):', JSON.stringify(carsArray));